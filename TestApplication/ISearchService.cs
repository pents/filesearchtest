﻿using System.Threading;
using System.Threading.Tasks;

namespace TestApplication
{
    public delegate void TreeUpdate(string path);
    public delegate void StatusUpdate(string currentFile, bool isSearchEnded);


    public interface ISearch
    {
        event TreeUpdate OnTreeUpdate;
        event StatusUpdate OnStatusUpdate;
        Task<int> StartSearch(string folder, string pattern, string searchText, CancellationTokenSource cancel, EventWaitHandle wait);
        void CancelSearch();
    }
}
