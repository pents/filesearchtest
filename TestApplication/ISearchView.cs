﻿using System;
using System.Threading;
using System.Windows.Forms;


namespace TestApplication
{

    public interface ISearchView
    {
        event Action StartSearch;
        event Action CancelSearch;
        event Action PauseSearch;
        event Action Closing;

        SynchronizationContext context { get; }

        TreeView Tree { get; }
        ToolStripLabel FileLabel { get; }
        ToolStripLabel TimeLabel { get; }
        Button StartBtn { get; }
        Button CancelBtn { get; }
        Button PauseBtn { get; }
        TextBox Folder { get; }
        TextBox Pattern { get; }
        TextBox SearchText { get; }

        void ShowAlert(string title, string message);
        void Show();
               

        //void Show();
        //void Close();
    }



}
