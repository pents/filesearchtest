﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

using Timer = System.Timers.Timer;

namespace TestApplication
{
    public class SearchController 
    {
        private string _startBtnText = "Search";
        private string _cancelBtnText = "Cancel";
        private string _PauseBtnText = "Pause";
        private string _resumeBtnText = "Resume";

        private string _totalFilesText = "Proccesed files: ";
        private string _currentFileText = " Current file: ";

        // Update treeView every 30ms with _updateResultTimer 
        private readonly int _treeUpdateTime = 30;

        private int _totalFiles;

        private readonly ISearchView _view; // shearchform object
        private readonly ISearch _service;

        private ConcurrentQueue<string> _searchResultsPaths;
        private CancellationTokenSource _cancelSource;
        private EventWaitHandle _waitHandle;

        private Stopwatch _time;
        private Timer _timeCounter; // thread-safe timer
        private Timer _updateResultTimer;

        //private DateTime _startTime;
        //private DateTime _stopTime;

        public SearchController(ISearchView view, ISearch service)
        {
            _view = view;
            _service = service;
            _time = new Stopwatch();
            _view.StartSearch += ViewOnStartSearch;
            _view.CancelSearch += ViewOnCancelSearch;
            _view.PauseSearch += ViewOnPauseSearch;

            _service.OnTreeUpdate += path => { _searchResultsPaths.Enqueue(path); };

            _view.Closing +=
                            () =>
                            {
                                SettingsManager.Save(new SavedData()
                                {
                                    DefaultDirectory = _view.Folder.Text,
                                    FilePattern = _view.Pattern.Text,
                                    TextToSearch = _view.SearchText.Text
                                });
                            };

        }

        private void ViewOnStartSearch()
        {
            _time.Start();
            if (_view.StartBtn.Text.Equals(_resumeBtnText)) 
            {
                UpdatePauseBtn(_PauseBtnText, true);
                UpdateCancelBtn(_cancelBtnText, true);
                UpdateStartBtn(_startBtnText, false);

                _waitHandle.Set();
                
                return;
            }
            UpdatePauseBtn(_PauseBtnText, true);
            UpdateCancelBtn(_cancelBtnText, true);
            UpdateStartBtn(_startBtnText, false);
            StartSearch(_view.Folder.Text, _view.Pattern.Text, _view.SearchText.Text);
        }

        private void ViewOnCancelSearch()
        {
            if (_view.PauseBtn.Enabled.Equals(true))
            {
                UpdatePauseBtn(_PauseBtnText, false);
                UpdateCancelBtn(_cancelBtnText, false);
                UpdateStartBtn(_startBtnText, true);

                _waitHandle.Set();
                _time.Start();
                ResetSearch();
                return;
            }
            UpdatePauseBtn(_PauseBtnText, false);
            UpdateCancelBtn(_cancelBtnText, false);
            _time.Reset();
            ResetSearch();
        }

        private void ViewOnPauseSearch()
        {
            UpdatePauseBtn(_PauseBtnText, false);
            UpdateCancelBtn(_cancelBtnText, true);
            UpdateStartBtn(_resumeBtnText, true);

            _time.Stop();

            // EventWaitHandler pause
            _waitHandle.Reset();
            
        }

        private void OnStatusUpdate(string currentFile, bool isSearchEnded)
        {
            if (isSearchEnded)
            {
                ResetSearch();
            }
                

            if (isSearchEnded && _totalFiles == 0)
            {
                _view.ShowAlert("Files not found", "Check file pattern");
            }
                
            _totalFiles++;

            if (!_cancelSource.IsCancellationRequested)
            {
                UpdateLabels(_totalFiles, currentFile);
            }             
        }

        private void StartSearch(string folder, string pattern, string searchText)
        {
            _service.OnStatusUpdate += OnStatusUpdate;

            if (folder.Trim() == "" || pattern.Trim() == "" || searchText.Trim() == "")
            {
                _view.ShowAlert("Error", "Fill all fields");
                return;
            }

            SettingsManager.Save(new SavedData { DefaultDirectory = folder, FilePattern = pattern, TextToSearch = searchText });

            // turn off 'start' button, turn on cancel and pause buttons
            UpdateStartBtn(_startBtnText, false);
            UpdateCancelBtn(_cancelBtnText, true);
            UpdatePauseBtn(_PauseBtnText, true);

            // set new search params
            beginSearch();

            _searchResultsPaths = new ConcurrentQueue<string>();

            _timeCounter = new Timer(100);
            _timeCounter.Elapsed += OnTimerStatusUpdate;
            _timeCounter.Start();

            //_startTime = DateTime.Now;

            _updateResultTimer = new Timer(_treeUpdateTime);
            _updateResultTimer.Elapsed += OnResultsTimerUpdate;
            _updateResultTimer.Start();

             
            _cancelSource = new CancellationTokenSource();
            _waitHandle = new ManualResetEvent(initialState: true);
            // Start search thread

            Task.Run(() =>
            {
                try
                {
                    Task<int> search = _service.StartSearch(folder, pattern, searchText, _cancelSource, _waitHandle);
                }
                catch (OperationCanceledException)
                {
                }
                catch (Exception)
                {
                    _view.ShowAlert("Error", "Unknown search error");
                }

            });

        }

        private void beginSearch()
        {
            _view.Tree.Nodes.Clear();

            UpdateLabels(0, "-");

            _totalFiles = 0;

            if (_timeCounter != null) {_timeCounter.Stop(); _timeCounter = null;}
            if (_updateResultTimer != null) {_updateResultTimer.Stop(); _updateResultTimer = null;}
        }

        // Each time _updateResultTimer executes this function - it updates the treeview
        private void OnResultsTimerUpdate(object sender, ElapsedEventArgs e)
        {
            if (_searchResultsPaths.Count <= 0) return;

            string path;
            _searchResultsPaths.TryDequeue(out path);

            if (!string.IsNullOrWhiteSpace(path))
            {
                _view.context.Post(delegate { PopulateTree(_view.Tree, path, '\\'); }, null);

            }

        }

        private void OnTimerStatusUpdate(object sender, ElapsedEventArgs e)
        {
           // _stopTime = DateTime.Now;
           // TimeSpan time = _stopTime - _startTime;

            _view.context.Post(delegate { _view.TimeLabel.Text = _time.Elapsed.ToString(@"hh\:mm\:ss"); }, null);
        }

        //?
        //private void UpdateButton(string text, bool enabled, Button btn)
        //{
        //    _view.context.Post(delegate { btn.Text = text; btn.Enabled = enabled; btn.Refresh(); }, null);
        //}

        private void UpdateStartBtn(string text, bool enabled)
        {
            _view.context.Post(delegate{_view.StartBtn.Text = text; _view.StartBtn.Enabled = enabled; _view.StartBtn.Refresh();}, null);
        }

        private void UpdatePauseBtn(string text, bool enabled)
        {
            _view.context.Post(delegate { _view.PauseBtn.Text = text; _view.PauseBtn.Enabled = enabled; _view.PauseBtn.Refresh(); }, null);
        }

        private void UpdateCancelBtn(string text, bool enabled)
        {
            _view.context.Post(delegate { _view.CancelBtn.Text = text; _view.CancelBtn.Enabled = enabled; _view.CancelBtn.Refresh(); }, null);
        }

        private void UpdateLabels(int totalFiles, string currentFile)
        {
            _view.context.Post(delegate{_view.FileLabel.Text = _totalFilesText + totalFiles + _currentFileText + currentFile;}, null);
        }

        private void ResetSearch()
        {
            if (_cancelSource != null) {_service.CancelSearch();}
                
            _service.OnStatusUpdate -= OnStatusUpdate;

            UpdateStartBtn(_startBtnText, true);
            UpdateCancelBtn(_cancelBtnText, false);
            UpdatePauseBtn(_PauseBtnText, false);

            if (_timeCounter != null) {_timeCounter.Stop(); _timeCounter = null;}
        }

        private void PopulateTree(TreeView treeView, string path, char pathSeparator)
        {
            TreeNode lastNode = null;

            string subPathAgg = string.Empty;
            foreach (string subPath in path.Split(pathSeparator))
            {
                subPathAgg += subPath + pathSeparator;
                TreeNode[] nodes = treeView.Nodes.Find(subPathAgg, true);

                if (nodes.Length == 0)
                {
                    if (lastNode == null)
                    {
                        lastNode = treeView.Nodes.Add(subPathAgg, subPath, 0);
                    }
                    else
                    {
                        lastNode = lastNode.Nodes.Add(subPathAgg, subPath, 0);
                    }
                }
                else
                {
                    lastNode = nodes[0];
                }
                    
            }
        }


        public void Run()
        {
            var settings = SettingsManager.Load();

            _view.context.Post(delegate
                                        {
                                            _view.Folder.Text = settings.DefaultDirectory;
                                            _view.Folder.Refresh();

                                            _view.Pattern.Text = settings.FilePattern;
                                            _view.Pattern.Refresh();

                                            _view.SearchText.Text = settings.TextToSearch;
                                            _view.SearchText.Refresh();

                                        },
            null);

            _view.Show();

        }


    }
}
