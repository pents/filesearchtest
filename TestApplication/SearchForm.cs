﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestApplication
{
    public partial class SearchForm : Form, ISearchView
    {
        
        private SynchronizationContext _context;
        public SearchForm()
        {
            InitializeComponent();
            _context = SynchronizationContext.Current; 
            FormClosing += delegate { Closing?.Invoke(); };
        }

        public event Action StartSearch;  
        public event Action CancelSearch;
        public event Action PauseSearch;
        public new event Action Closing;

        // used to address form values from other thread
        public SynchronizationContext context
        {   
            get { return _context;}
        }

        public TreeView Tree
        {
            get { return treeView1; }
        }

        public ToolStripLabel FileLabel
        {
            get { return toolStripSummaryLabel; }
        }
        public ToolStripLabel TimeLabel
        {
            get { return toolStripTimeLabel; }
        }

        public TextBox Folder
        {
            get { return startDirTB; }
        }

        public TextBox Pattern
        {
            get { return fileTempTB; }
        }

        public TextBox SearchText
        {
            get { return searchTextTB; }
        }

        public Button StartBtn
        {
            get { return startSearchBtn; }
        }

        public Button CancelBtn
        {
            get { return cancelSearchBtn; }
        }

        public Button PauseBtn
        {
            get { return pauseSearchBtn; }
        }
        public void ShowAlert(string title, string message)
        {
            MessageBox.Show(message, title);
        }

        public new void Show()
        {
            Application.Run(this);
        }

        private void startSearchBtn_Click(object sender, EventArgs e)
        {
            StartSearch?.Invoke();
        }

        private void cancelSearchBtn_Click(object sender, EventArgs e)
        {
            CancelSearch?.Invoke();
        }

        private void pauseSearchBtn_Click(object sender, EventArgs e)
        {
            PauseSearch?.Invoke();
        }

        private void selectDirBtn_Click(object sender, EventArgs e)
        {
            var openFileDialog = new FolderBrowserDialog();

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                startDirTB.Text = openFileDialog.SelectedPath;
            }
        }


    }
}
