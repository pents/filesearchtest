﻿using System;
using System.IO;
using System.Linq.Expressions;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace TestApplication
{
    public class SavedData
    {
        public string DefaultDirectory { get; set; }
        public string FilePattern { get; set; }
        public string TextToSearch { get; set; }

    }

    public static class SettingsManager
    {
        private const string SettingsFile = "settings.ini";

        public static void Save(SavedData saveData)
        {

            using (TextWriter writer = File.CreateText(Path.Combine(Application.StartupPath, SettingsFile)))
            {
                writer.Write(JsonConvert.SerializeObject(saveData));
            }
        }

        public static SavedData Load()
        {
            try
            {
                using (TextReader reader = File.OpenText(Path.Combine(Application.StartupPath, SettingsFile)))
                {
                    return JsonConvert.DeserializeObject<SavedData>(reader.ReadLine());
                }
            }
            catch (FileNotFoundException)
            {
                string folderPath = Directory.GetCurrentDirectory();

                return new SavedData { DefaultDirectory = folderPath, FilePattern = "*.txt", TextToSearch = "a" };
            }


        }


    }
}
