﻿namespace TestApplication
{
    partial class SearchForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripStatus = new System.Windows.Forms.ToolStrip();
            this.toolStripTimeLabel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSummaryLabel = new System.Windows.Forms.ToolStripLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.cancelSearchBtn = new System.Windows.Forms.Button();
            this.pauseSearchBtn = new System.Windows.Forms.Button();
            this.selectDirBtn = new System.Windows.Forms.Button();
            this.startSearchBtn = new System.Windows.Forms.Button();
            this.searchTextTB = new System.Windows.Forms.TextBox();
            this.fileTempTB = new System.Windows.Forms.TextBox();
            this.searchTextLabel = new System.Windows.Forms.Label();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.startDirTB = new System.Windows.Forms.TextBox();
            this.toolStripStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripStatus
            // 
            this.toolStripStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStripStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTimeLabel,
            this.toolStripSummaryLabel});
            this.toolStripStatus.Location = new System.Drawing.Point(0, 374);
            this.toolStripStatus.Name = "toolStripStatus";
            this.toolStripStatus.Size = new System.Drawing.Size(875, 25);
            this.toolStripStatus.TabIndex = 1;
            this.toolStripStatus.Text = "toolStripStatus";
            // 
            // toolStripTimeLabel
            // 
            this.toolStripTimeLabel.Name = "toolStripTimeLabel";
            this.toolStripTimeLabel.Size = new System.Drawing.Size(49, 22);
            this.toolStripTimeLabel.Text = "00:00:00";
            // 
            // toolStripSummaryLabel
            // 
            this.toolStripSummaryLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSummaryLabel.Name = "toolStripSummaryLabel";
            this.toolStripSummaryLabel.Size = new System.Drawing.Size(103, 22);
            this.toolStripSummaryLabel.Text = "toolStripSummary";
            this.toolStripSummaryLabel.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.cancelSearchBtn);
            this.splitContainer1.Panel2.Controls.Add(this.pauseSearchBtn);
            this.splitContainer1.Panel2.Controls.Add(this.selectDirBtn);
            this.splitContainer1.Panel2.Controls.Add(this.startSearchBtn);
            this.splitContainer1.Panel2.Controls.Add(this.searchTextTB);
            this.splitContainer1.Panel2.Controls.Add(this.fileTempTB);
            this.splitContainer1.Panel2.Controls.Add(this.searchTextLabel);
            this.splitContainer1.Panel2.Controls.Add(this.fileNameLabel);
            this.splitContainer1.Panel2.Controls.Add(this.startDirTB);
            this.splitContainer1.Size = new System.Drawing.Size(875, 374);
            this.splitContainer1.SplitterDistance = 290;
            this.splitContainer1.TabIndex = 2;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(290, 374);
            this.treeView1.TabIndex = 0;
            // 
            // cancelSearchBtn
            // 
            this.cancelSearchBtn.Enabled = false;
            this.cancelSearchBtn.Location = new System.Drawing.Point(2, 110);
            this.cancelSearchBtn.Name = "cancelSearchBtn";
            this.cancelSearchBtn.Size = new System.Drawing.Size(153, 39);
            this.cancelSearchBtn.TabIndex = 9;
            this.cancelSearchBtn.Text = "Cancel";
            this.cancelSearchBtn.UseVisualStyleBackColor = true;
            this.cancelSearchBtn.Click += new System.EventHandler(this.cancelSearchBtn_Click);
            // 
            // pauseSearchBtn
            // 
            this.pauseSearchBtn.Enabled = false;
            this.pauseSearchBtn.Location = new System.Drawing.Point(222, 110);
            this.pauseSearchBtn.Name = "pauseSearchBtn";
            this.pauseSearchBtn.Size = new System.Drawing.Size(142, 39);
            this.pauseSearchBtn.TabIndex = 8;
            this.pauseSearchBtn.Text = "Pause";
            this.pauseSearchBtn.UseVisualStyleBackColor = true;
            this.pauseSearchBtn.Click += new System.EventHandler(this.pauseSearchBtn_Click);
            // 
            // selectDirBtn
            // 
            this.selectDirBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectDirBtn.Location = new System.Drawing.Point(2, 12);
            this.selectDirBtn.Name = "selectDirBtn";
            this.selectDirBtn.Size = new System.Drawing.Size(576, 23);
            this.selectDirBtn.TabIndex = 7;
            this.selectDirBtn.Text = "Select folder";
            this.selectDirBtn.UseVisualStyleBackColor = true;
            this.selectDirBtn.Click += new System.EventHandler(this.selectDirBtn_Click);
            // 
            // startSearchBtn
            // 
            this.startSearchBtn.Location = new System.Drawing.Point(428, 110);
            this.startSearchBtn.Name = "startSearchBtn";
            this.startSearchBtn.Size = new System.Drawing.Size(149, 39);
            this.startSearchBtn.TabIndex = 6;
            this.startSearchBtn.Text = "Search";
            this.startSearchBtn.UseVisualStyleBackColor = true;
            this.startSearchBtn.Click += new System.EventHandler(this.startSearchBtn_Click);
            // 
            // searchTextTB
            // 
            this.searchTextTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchTextTB.Location = new System.Drawing.Point(70, 84);
            this.searchTextTB.Name = "searchTextTB";
            this.searchTextTB.Size = new System.Drawing.Size(507, 20);
            this.searchTextTB.TabIndex = 5;
            // 
            // fileTempTB
            // 
            this.fileTempTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileTempTB.Location = new System.Drawing.Point(94, 58);
            this.fileTempTB.Name = "fileTempTB";
            this.fileTempTB.Size = new System.Drawing.Size(484, 20);
            this.fileTempTB.TabIndex = 4;
            // 
            // searchTextLabel
            // 
            this.searchTextLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchTextLabel.AutoSize = true;
            this.searchTextLabel.Location = new System.Drawing.Point(3, 87);
            this.searchTextLabel.Name = "searchTextLabel";
            this.searchTextLabel.Size = new System.Drawing.Size(61, 13);
            this.searchTextLabel.TabIndex = 3;
            this.searchTextLabel.Text = "Search text";
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileNameLabel.AutoSize = true;
            this.fileNameLabel.Location = new System.Drawing.Point(3, 61);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(85, 13);
            this.fileNameLabel.TabIndex = 2;
            this.fileNameLabel.Text = "Filename pattern";
            // 
            // startDirTB
            // 
            this.startDirTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.startDirTB.Location = new System.Drawing.Point(3, 34);
            this.startDirTB.Name = "startDirTB";
            this.startDirTB.ReadOnly = true;
            this.startDirTB.Size = new System.Drawing.Size(575, 20);
            this.startDirTB.TabIndex = 1;
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(875, 399);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStripStatus);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "File searcher";
            this.toolStripStatus.ResumeLayout(false);
            this.toolStripStatus.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripStatus;
        private System.Windows.Forms.ToolStripLabel toolStripTimeLabel;
        private System.Windows.Forms.ToolStripLabel toolStripSummaryLabel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button startSearchBtn;
        private System.Windows.Forms.TextBox searchTextTB;
        private System.Windows.Forms.TextBox fileTempTB;
        private System.Windows.Forms.Label searchTextLabel;
        private System.Windows.Forms.Label fileNameLabel;
        private System.Windows.Forms.TextBox startDirTB;
        private System.Windows.Forms.Button selectDirBtn;
        private System.Windows.Forms.Button cancelSearchBtn;
        private System.Windows.Forms.Button pauseSearchBtn;
    }
}

