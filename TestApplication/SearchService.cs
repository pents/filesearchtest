﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace TestApplication
{
    public class SearchService : ISearch
    {
        private CancellationTokenSource _cancelSource;
        private EventWaitHandle _waitHandle;

        public event TreeUpdate OnTreeUpdate;
        public event StatusUpdate OnStatusUpdate;

        private int _proceededFiles;

        public async Task<int> StartSearch(string path, string pattern, string searchText, CancellationTokenSource cancelSource, EventWaitHandle waitHandle)
        {

            if (path.Trim() == "" || pattern.Trim() == "" || searchText.Trim() == "" )
            {
                throw new ArgumentNullException("Argument is not filled");
            }

            // Cancel request
            _cancelSource = cancelSource;
            _waitHandle = waitHandle;

            _proceededFiles = 0;

            await GetDirectoryAsync(new DirectoryInfo(path), pattern, searchText, _cancelSource.Token);

            OnStatusUpdate?.Invoke("Search finish", true);

            return _proceededFiles;
        }

        private async Task GetDirectoryAsync(DirectoryInfo rootFolder, string pattern, string searchText, CancellationToken cancellation)
        {
            FileInfo[] files = null;
            DirectoryInfo[] subDirs;

            try
            {
                files = rootFolder.GetFiles(pattern);
            }
            catch (Exception e) { }


            if (files != null)
            {
                foreach (FileInfo fi in files)
                {
                    _waitHandle.WaitOne();
                    if (fi.Directory != null)
                    {
                        await Task.Run(() => ProcessFileAsync(fi.FullName, searchText, cancellation) );
                    }
                }
   
                subDirs = rootFolder.GetDirectories();

                foreach (DirectoryInfo dirInfo in subDirs)
                {
                    _waitHandle.WaitOne();
                    await Task.Run(() => GetDirectoryAsync(dirInfo, pattern, searchText, cancellation));
                }

            }
        }

        private async Task ProcessFileAsync(string path, string searchText, CancellationToken cancelToken)
        {
            // Cancellation if requested
            if (cancelToken.IsCancellationRequested)
            {
                await Task.FromResult(false);
            }

            // Add file info if search text were found
            string fileName = Path.GetFileName(path);

            string searchString = searchText.Trim();

            if (File.ReadLines(path).Any(line => line.Contains(searchString)) && !cancelToken.IsCancellationRequested)
            {
                ResultFound(path);
            }


            // Update counter
            if (OnStatusUpdate != null && !cancelToken.IsCancellationRequested)
            {
                OnStatusUpdate(fileName, false);
            }


            _proceededFiles++;
            await Task.FromResult(true);

        }

        private void ResultFound(string path)
        {
            OnTreeUpdate?.Invoke(path);
        }

        public void CancelSearch()
        {
            _cancelSource?.Cancel();
        }



    }
}
